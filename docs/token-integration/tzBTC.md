# tzBTC

tzBTC is a stable coin token that represents BTC on the Tezos blockchain in a
tradeable form. It adheres to the [FA1.2 specification](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md).
The source used to output tzBTC is located [here](https://github.com/tz-wrapped/tezos-btc).

The two contracts of interest are [tzBTC](https://better-call.dev/mainnet/KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn) and
[Dexter tzBTC/XTZ](https://better-call.dev/mainnet/KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf).

For more information, visit the [tzBTC website](https://tzbtc.io/) and the 
[Bitcoin Association Switzerland website](https://www.bitcoinassociation.ch/tzbtc).

https://github.com/tz-wrapped/tezos-btc/tree/autodoc/master
https://github.com/tz-wrapped/tezos-btc/blob/autodoc/master/TZBTC-contract.md

# Token integration checklist

Each token contract must be evaluated individually before originating a new Dexter 
exchange contract for it, you need to consider the risks associated with it and 
provide this knowledge publicly. FA1.2 is a light  specification on the required 
behavior of a token contract. It leaves a lot of room for behavior that would be 
incompatible with creating a safe Dexter exchange. 

For each FA1.2 contract integrated into a Dexter exchange contract, you 
should publish and link this checklist filled out with the details for it. 
Add detailed comments for each question below.

Anything not checked is considered a vulnerability. Having a vulnerability
does not mean that the Dexter exchange contract should not be created, rather it
is to warn users of possible weaknesses of the FA1.2 contract. It is up to them to
decide whether or not to support the contract.

This document is based off of Trail of Bits' 
[Ethereum token integration checklist](https://github.com/crytic/building-secure-contracts/blob/c6ff933c0ce56e16edaaaf2160f01fd2620d52e1/development-guidelines/token_integration.md).

## General considerations

- [ ] **The FA1.2 contract has a security review.** Avoid interacting with contracts 
  that have not had a security review.

To our knowledge there has not been a formal security review of the tzBTC 
contract code.

- [x] **The FA1.2 contract is documented.** It should explain how the contract works
  and how to interact with it.

This [document](https://github.com/tz-wrapped/tezos-btc/blob/autodoc/master/TZBTC-contract.md) explains
how the contract works.

- [x] **There are clear steps how to originate the contract on the Tezos test net.**

The [tzBTC readme](https://github.com/tz-wrapped/tezos-btc#readme) has details
on how to originate the contract.

- [ ] **Contact the developers of the FA1.2 contract.** There maybe known issues 
  that they have not documented. There may be peculiar behaviors that you should
  know about.

Contact is pending.

- [ ] **They have a security mailing list for critical announcements.** Their team 
  should advise users (like you!) when critical issues are found or when 
  upgrades occur.

There is not a mailling list that we know of.

## FA1.2 conformity


- [x] **The FA1.2 contract conforms to all of the specifications of 
  [tzip-7](https://gitlab.com/tzip/tzip/-/blob/d53952592717ef56ce45cc80d7231bec5c12592b/proposals/tzip-7/tzip-7.md)
  (The FA1.2 specification).**

## Contract composition

- [x] **Do not interface Dexter with FA1.2 
  contracts that perform internal operations when getBalance is called.**
  Dexter uses the getBalance entrypoint of FA1.2 internally. Due to the 
  message passing architecture of Tezos (https://forum.tezosagora.org/t/smart-contract-vulnerabilities-due-to-tezos-message-passing-architecture/2045)
  which allows for reordering of internal operations, Dexter can only safely call 
  getBalance of FA1.2 tokens that forward this value  without
  performing calls to other contracts (For example, the FA1.2 contract is 
  composed of two contracts and must query the second contract to calculate the balance). 
  Such a contract would allow attackers to nsert operations and potentially change the 
  expected forwarded value of getBalance. 

- [x] **Do not interface Dexter with FA1.2 contracts that perform internal 
  operations when transfer is called**. Moreover, transfer should immediately
  update the balances when called. Same reasoning as above.
  
- [x] **If the contract was not written in Michelson, the 
  author should publish the code that was used to generate the Michelson 
  contract that was originated on Tezos.**

The source code is available [here](https://github.com/tz-wrapped/tezos-btc).

- [ ] **The contract code is well commented.** It should be relatively 
  straightforward for experienced contract developers to understand
  the purpose of the code in the contract. The contract avoids unnecessary 
  complexity.

The readmes explain how to originate and interact with the contract, and 
throughly explains the features of the smart contract, but there are not 
a lot of comments within the code.

## Owner privileges

- [ ] **The token is not upgradeable.** Upgradeable contracts might change their 
  rules over time.
  
The tzBTC contract is upgradeable. There are two documents that explain it: 
[contract upgradeability](https://github.com/tz-wrapped/tezos-btc/blob/autodoc/master/TZBTC-contract.md#contract-upgradeability) and
[administrator forced upgrades](https://gitlab.com/morley-framework/morley/-/blob/a30dddb633ee880761c3cbf1d4a69ee040ffad25/docs/upgradeableContracts.md#section-2-administrator-forced-upgrades).

- [ ] **The token is not pausable.** Malicious or compromised owners can trap 
  contracts relying on pausable tokens. Identify pauseable code by hand.

tzBTC is pausable. Tokens can be minted and burned.

- [ ] **The owner cannot blacklist the contract.** Malicious or compromised 
  owners can trap contracts relying on tokens with a blacklist. Identify 
  blacklisting features by hand.

Need to confirm with the team behind the develoeprs.

- [x] **The team behind the token is known and can be held responsible for 
  abuse.** Contracts with anonymous development teams, or that reside in 
  legal shelters should require a higher standard of review.

The [tzBTC website](https://tzbtc.io/) and the 
[Bitcoin Association Switzerland website](https://www.bitcoinassociation.ch/tzbtc)
are very clear about who the responsible parties are.

## Token scarcity

- [ ] **No user owns most of the supply.** If a few users own most of the tokens, 
  they can influence operations based on the token's repartition.

The addresses that own tzBTC are visible here: 
[tzBTC token owners](https://better-call.dev/mainnet/KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn/tokens).

- [x] **The total supply is sufficient.** Tokens with a low total supply can be 
  easily manipulated. The lower the balance of token or XTZ in a dexter exchange
  contract, the easier it is to manipulate the exchange rate. If there is a low
  total supply, then the maximum token pool will be low. In our experience, token
  pools of less than 100,000 in Dexter are easy to manipulate. Depending on the
  nature of the contract, a total supply of 100,000 is likely to low, if it is
  300,000 and the majority will be in Dexter, than that is good, otherwise consider
  how much is likely to end up in Dexter.

There is a sufficient number of tokens.

- [x] **The tokens are located in more than a few exchanges.** If all the tokens are 
  in one exchange, a compromise of the exchange can compromise the contract 
  relying on the token.

There are multiple providers of tzBTC.

- [x] **The token does not allow flash minting.** Flash minting can lead to
  substantial swings in the balance and the total supply, which 
  neccessitate strict and comprehensive overflow checks in the operation of the 
  token.

Minting must go through confirmed providers.
