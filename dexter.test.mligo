(* These tests require a recent development version of LIGO.

   To run:

   With LIGO built from source (preferably at SHA 2ae7cfa364.)
   [ligo test dexter.test.mligo]

   Using docker:
   [docker run --rm -v $PWD:$PWD -w $PWD ligolang/ligo:next test dexter.test.mligo] *)


(*****************************************************************************)
(* Definitions and helpers                                                   *)
(*****************************************************************************)


#include "./lqt_fa12.mligo"
let lqt_main = main
type lqt_storage = storage
type lqt_param = parameter
#include "./dexter.mligo"
let dex_main = main
type dex_storage = storage
type dex_param = entrypoint


let past = ("2009-09-09T09:09:09Z" : timestamp)
let now = ("2010-10-10T10:10:10Z" : timestamp)
let future = ("2011-11-11T11:11:11Z" : timestamp)

let other () = Test.nth_bootstrap_account 0
let src () = Test.nth_bootstrap_account 1
let delegate () = Test.nth_bootstrap_account 2

type deploy_params =
  { tok : nat ;
    lqt : nat ;
    xtz : tez ;
    lqt_provider : address ;
    provider_tok : nat ;
    dex_manager : address }

type deploy_result =
  { dex_addr : (dex_param, dex_storage) typed_address ;
    lqt_addr : (lqt_param, lqt_storage) typed_address ;
    tok_addr : (lqt_param, lqt_storage) typed_address }

let approve_allowance (deployed : deploy_result) (allowance : nat) =
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "approve" deployed.tok_addr : approve contract)
    { spender = Tezos.address (Test.to_contract deployed.dex_addr) ;
      value = allowance }
    0tz

let deploy (params : deploy_params) =
  (* originate tok (reusing lqt_fa12 here) *)
  let tok_storage =
    { tokens = (Big_map.literal [(params.lqt_provider, params.provider_tok + params.tok)] : tokens) ;
      allowances = (Big_map.empty : allowances) ;
      (* src as admin so we can use mintOrBurn in
         test_update_token_pool *)
      admin = src () ;
      total_supply = params.provider_tok + params.tok } in
  let (tok_addr, _, _) = Test.originate lqt_main tok_storage 0tz in

  (* originate dex *)
  let dex_storage =
    build_storage
      {lqtTotal = params.lqt ;
       manager = params.dex_manager ;
       tokenAddress = Tezos.address (Test.to_contract tok_addr)} in
  let (dex_addr, _, _) = Test.originate dex_main dex_storage params.xtz in

  (* originate lqt *)
  let lqt_storage =
    { tokens = (Big_map.literal [(params.lqt_provider, params.lqt)] : tokens) ;
      allowances = (Big_map.empty : allowances) ;
      admin = Tezos.address (Test.to_contract dex_addr) ;
      total_supply = params.lqt } in
  let (lqt_addr, _, _) = Test.originate lqt_main lqt_storage 0tz in

  (* provide liquidity *)
  let () =
    Test.transfer_to_contract_exn
      (Test.to_contract dex_addr : unit contract)
      ()
      params.xtz in
  let () =
    Test.transfer_to_contract_exn
      (Test.to_entrypoint "transfer" tok_addr : transfer contract)
      { address_from = src () ;
        address_to = Tezos.address (Test.to_contract dex_addr) ;
        value = params.tok }
      0tz in
  let () =
    Test.transfer_to_contract_exn
      (Test.to_entrypoint "updateTokenPool" dex_addr : unit contract)
      ()
      0tz in

  (* set lqt address in dex *)
  let () =
    Test.transfer_to_contract_exn
      (Test.to_entrypoint "setLqtAddress" dex_addr : address contract)
      (Tezos.address (Test.to_contract lqt_addr))
      0tz in

  { dex_addr = dex_addr ;
    lqt_addr = lqt_addr ;
    tok_addr = tok_addr }


type deploy_selfIsUpdatingTokenPool_params =
  { lqt : nat ;
    dex_manager : address }

(* Similar to deploy, but deploys a dex where selfIsUpdatingTokenPool is true.
   Unlike deploy, it does not set up the pools. *)
let deploy_selfIsUpdatingTokenPool (params : deploy_selfIsUpdatingTokenPool_params) =
  (* originate tok (reusing lqt_fa12 here) *)
  let tok_storage =
    { tokens = (Big_map.empty : tokens) ;
      allowances = (Big_map.empty : allowances) ;
      (* src as admin so we can use mintOrBurn in
         test_update_token_pool *)
      admin = src () ;
      total_supply = 0n } in
  let (tok_addr, _, _) = Test.originate lqt_main tok_storage 0tz in

  (* originate dex *)
  let dex_storage =
    build_storage
      {lqtTotal = params.lqt ;
       manager = params.dex_manager ;
       tokenAddress = Tezos.address (Test.to_contract tok_addr)} in
  let dex_storage = { dex_storage with selfIsUpdatingTokenPool = true } in
  let (dex_addr, _, _) = Test.originate dex_main dex_storage 0mutez in

  (* originate lqt *)
  let lqt_storage =
    { tokens = (Big_map.empty : tokens) ;
      allowances = (Big_map.empty : allowances) ;
      admin = Tezos.address (Test.to_contract dex_addr) ;
      total_supply = 0n } in
  let (lqt_addr, _, _) = Test.originate lqt_main lqt_storage 0tz in

  { dex_addr = dex_addr ;
    lqt_addr = lqt_addr ;
    tok_addr = tok_addr }

let clean () =
  begin
    Test.reset_state 3n [10000000000n; 10000000000n; 10000000000n];
    Test.set_now now
  end

let basic_setup () =
  let () = clean () in
  let () = Test.set_source (src ()) in
  let deployed =
    deploy { lqt = 1000000n ;
             tok = 1000000n ;
             xtz = 1tz ;
             lqt_provider = src () ;
             provider_tok = 1000000000000n ;
             dex_manager = src () } in
  let () = approve_allowance deployed 1000000000000n in
  deployed

let basic_setup_selfIsUpdatingTokenPool () =
  let () = clean () in
  let () = Test.set_source (src ()) in
  let params = {
    lqt = 1000000n ;
    dex_manager = src () ;
  } in
  let deployed = deploy_selfIsUpdatingTokenPool params in
  deployed


(* dex entrypoint helpers *)

let do_add_liquidity
    (deployed : deploy_result)
    (add_liquidity : add_liquidity)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract
    (Test.to_entrypoint "addLiquidity" dex_addr : add_liquidity contract)
    add_liquidity
    amt

let assert_add_liquidity
    (deployed : deploy_result)
    (add_liquidity : add_liquidity)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "addLiquidity" dex_addr : add_liquidity contract)
    add_liquidity
    amt

let do_remove_liquidity
    (deployed : deploy_result)
    (remove_liquidity : remove_liquidity)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract
    (Test.to_entrypoint "removeLiquidity" dex_addr : remove_liquidity contract)
    remove_liquidity
    amt

let assert_remove_liquidity
    (deployed : deploy_result)
    (remove_liquidity : remove_liquidity) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "removeLiquidity" dex_addr : remove_liquidity contract)
    remove_liquidity
    0tz

let do_xtz_to_token
    (deployed : deploy_result)
    (xtz_to_token : xtz_to_token)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract
    (Test.to_entrypoint "xtzToToken" dex_addr : xtz_to_token contract)
    xtz_to_token
    amt

let assert_xtz_to_token
    (deployed : deploy_result)
    (xtz_to_token : xtz_to_token)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "xtzToToken" dex_addr : xtz_to_token contract)
    xtz_to_token
    amt

let do_token_to_xtz
    (deployed : deploy_result)
    (token_to_xtz : token_to_xtz)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract
    (Test.to_entrypoint "tokenToXtz" dex_addr : token_to_xtz contract)
    token_to_xtz
    amt

let assert_token_to_xtz
    (deployed : deploy_result)
    (token_to_xtz : token_to_xtz) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "tokenToXtz" dex_addr : token_to_xtz contract)
    token_to_xtz
    0tz

let do_set_baker
    (deployed : deploy_result)
    (set_baker : set_baker)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract
    (Test.to_entrypoint "setBaker" dex_addr : set_baker contract)
    set_baker
    amt

let assert_set_baker
    (deployed : deploy_result)
    (set_baker : set_baker) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "setBaker" dex_addr : set_baker contract)
    set_baker
    0tz

let do_set_manager
    (deployed : deploy_result)
    (manager : address)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract
    (Test.to_entrypoint "setManager" dex_addr : address contract)
    manager
    amt

let do_set_lqt_address
    (deployed : deploy_result)
    (lqt_address : address)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract
    (Test.to_entrypoint "setLqtAddress" dex_addr : address contract)
    lqt_address
    amt

let assert_set_manager
    (deployed : deploy_result)
    (manager : address) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "setManager" dex_addr : address contract)
    manager
    0tz

let do_update_token_pool
    (deployed : deploy_result)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract
    (Test.to_entrypoint "updateTokenPool" dex_addr : unit contract)
    ()
    amt


let do_update_token_pool_internal
    (deployed : deploy_result)
    (token_pool : update_token_pool_internal)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract
    (Test.to_entrypoint "updateTokenPoolInternal" dex_addr : update_token_pool_internal contract)
    token_pool
    amt

let assert_update_token_pool
    (deployed : deploy_result) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "updateTokenPool" dex_addr : unit contract)
    ()
    0tz

let do_token_to_token
    (deployed : deploy_result)
    (token_to_token : token_to_token)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract
    (Test.to_entrypoint "tokenToToken" dex_addr : token_to_token contract)
    token_to_token
    amt

let assert_token_to_token
    (deployed : deploy_result)
    (token_to_token : token_to_token) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "tokenToToken" dex_addr : token_to_token contract)
    token_to_token
    0tz


(* assertion helpers *)

let assert_error (error_code : nat) (result : test_exec_result) : unit =
  match result with
  | Success -> failwith "expected error, got success"
  | Fail e ->
    (match e with
     | Rejected (err, _) ->
       let error_code = Test.compile_value error_code in
       if Test.michelson_equal err error_code
       then ()
       else failwith "wrong error" (* sad *)
     | Other ->
       failwith "other error" (* sad *))

type pools =
  { xtz_pool : tez ;
    token_pool : nat ;
    lqt_total : nat }

let assert_dex_pools (deployed : deploy_result) (pools : pools) : unit =
  let { dex_addr } = deployed in
  let storage = Test.get_storage dex_addr in
  begin
    if storage.xtzPool = pools.xtz_pool then () else failwith "incorrect xtzPool";
    if storage.tokenPool = pools.token_pool then () else failwith storage.tokenPool;
    if storage.lqtTotal = pools.lqt_total then () else failwith storage.lqtTotal
  end

let assert_tokens
    (tok_addr : (lqt_param, lqt_storage) typed_address)
    (addr : address) (tok : nat) : unit =
  let storage = Test.get_storage tok_addr in
  let actual_tok = Big_map.find_opt addr storage.tokens in
  let actual_tok =
    match actual_tok with
    | None -> 0n
    | Some actual_tok -> actual_tok in
  if tok = actual_tok
  then ()
  else failwith actual_tok

let assert_tok
    (deployed : deploy_result)
    (addr : address) (tok : nat) : unit =
  let { tok_addr } = deployed in
  assert_tokens tok_addr addr tok

let assert_lqt
    (deployed : deploy_result)
    (addr : address) (lqt : nat) : unit =
  let { lqt_addr } = deployed in
  assert_tokens lqt_addr addr lqt

(*****************************************************************************)
(* Tests                                                                     *)
(*****************************************************************************)
let test_setup =
  let deployed = basic_setup () in
  begin
    assert_dex_pools
      deployed
      { xtz_pool = 1tz ;
        token_pool = 1000000n ;
        lqt_total = 1000000n } ;
    assert_tok deployed (src ()) 1000000000000n ;
    assert_lqt deployed (src ()) 1000000n
  end

(* add liquidity *)

let test_add_liquidity =
  let deployed = basic_setup () in
  begin
    assert_add_liquidity
      deployed
      { owner = src () ;
        minLqtMinted = 1000000n ;
        maxTokensDeposited = 1000000n ;
        deadline = future }
      1tz ;
    assert_dex_pools
      deployed
      { xtz_pool = 2tz ;
        token_pool = 2000000n ;
        lqt_total = 2000000n } ;
    assert_tok deployed (src ()) 999999000000n ;
    assert_lqt deployed (src ()) 2000000n
  end

let test_add_more_liquidity =
  let deployed = basic_setup () in
  begin
    assert_add_liquidity
      deployed
      { owner = src () ;
        minLqtMinted = 2000000n ;
        maxTokensDeposited = 2000000n ;
        deadline = future }
      2tz ;
    assert_dex_pools
      deployed
      { xtz_pool = 3tz ;
        token_pool = 3000000n ;
        lqt_total = 3000000n } ;
    assert_tok deployed (src ()) 999998000000n ;
    assert_lqt deployed (src ()) 3000000n
  end

let test_add_liquidity_error_deadline =
  let deployed = basic_setup () in
  assert_error
    error_THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE
    (do_add_liquidity
       deployed
       { owner = src () ;
         minLqtMinted = 1000000n ;
         maxTokensDeposited = 1000000n ;
         deadline = past }
       1tz)

let test_add_liquidity_error_maxTokensDeposited =
  let deployed = basic_setup () in
  assert_error
    error_MAX_TOKENS_DEPOSITED_MUST_BE_GREATER_THAN_OR_EQUAL_TO_TOKENS_DEPOSITED
    (do_add_liquidity
       deployed
       { owner = src () ;
         minLqtMinted = 1000000n ;
         maxTokensDeposited = 999999n ;
         deadline = future }
       1tz)

let test_add_liquidity_error_minLqtMinted =
  let deployed = basic_setup () in
  assert_error
    error_LQT_MINTED_MUST_BE_GREATER_THAN_MIN_LQT_MINTED
    (do_add_liquidity
       deployed
       { owner = src () ;
         minLqtMinted = 1000001n ;
         maxTokensDeposited = 1000000n ;
         deadline = future }
       1tz)

let test_add_liquidity_error_selfIsUpdatingTokenPool =
  let deployed = basic_setup_selfIsUpdatingTokenPool () in
  assert_error
    error_SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE
    (do_add_liquidity
       deployed
       { owner = src () ;
         minLqtMinted = 1000000n ;
         maxTokensDeposited = 1000000n ;
         deadline = now }
       1tz)

(* remove liquidity *)

let test_remove_liquidity =
  let deployed = basic_setup () in
  begin
    assert_remove_liquidity
      deployed
      { to_ = src () ;
        lqtBurned = 500000n ;
        minXtzWithdrawn = 500000mutez ;
        minTokensWithdrawn = 500000n ;
        deadline = future } ;
    assert_dex_pools
      deployed
      { xtz_pool = 500000mutez ;
        token_pool = 500000n ;
        lqt_total = 500000n } ;
    assert_tok deployed (src ()) 1000000500000n ;
    assert_lqt deployed (src ()) 500000n
  end

let test_remove_liquidity_error_deadline =
  let deployed = basic_setup () in
  assert_error
    error_THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE
    (do_remove_liquidity
       deployed
       { to_ = src () ;
         lqtBurned = 500000n ;
         minXtzWithdrawn = 500000mutez ;
         minTokensWithdrawn = 500000n ;
         deadline = past }
       0tz)

let test_remove_liquidity_error_minXtzWithdrawn =
  let deployed = basic_setup () in
  assert_error
    error_THE_AMOUNT_OF_XTZ_WITHDRAWN_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_XTZ_WITHDRAWN
    (do_remove_liquidity
       deployed
       { to_ = src () ;
         lqtBurned = 500000n ;
         minXtzWithdrawn = 500001mutez ;
         minTokensWithdrawn = 500000n ;
         deadline = future }
       0tz)

let test_remove_liquidity_error_minTokensWithdrawn =
  let deployed = basic_setup () in
  assert_error
    error_THE_AMOUNT_OF_TOKENS_WITHDRAWN_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_TOKENS_WITHDRAWN
    (do_remove_liquidity
       deployed
       { to_ = src () ;
         lqtBurned = 500000n ;
         minXtzWithdrawn = 500000mutez ;
         minTokensWithdrawn = 500001n ;
         deadline = future }
       0tz)

let test_remove_liquidity_error_too_much_burn =
  let deployed = basic_setup () in
  assert_error
    error_CANNOT_BURN_MORE_THAN_THE_TOTAL_AMOUNT_OF_LQT
    (do_remove_liquidity
       deployed
       { to_ = src () ;
         lqtBurned = 1000001n ;
         minXtzWithdrawn = 500000mutez ;
         minTokensWithdrawn = 500000n ;
         deadline = future }
       0tz)

let test_remove_liquidity_error_selfIsUpdatingTokenPool =
  let deployed = basic_setup_selfIsUpdatingTokenPool () in
  assert_error
    error_SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE
    (do_remove_liquidity
       deployed
       { to_ = src () ;
         lqtBurned = 500000n ;
         minXtzWithdrawn = 500000mutez ;
         minTokensWithdrawn = 500000n ;
         deadline = future }
       0tz)

let test_remove_liquidity_error_amount =
  let deployed = basic_setup () in
  assert_error
    error_AMOUNT_MUST_BE_ZERO
    (do_remove_liquidity
       deployed
       { to_ = src () ;
         lqtBurned = 500000n ;
         minXtzWithdrawn = 500000mutez ;
         minTokensWithdrawn = 500000n ;
         deadline = future }
       1tz)

(* xtz to token *)

let test_xtz_to_token =
  let deployed = basic_setup () in
  begin
    assert_xtz_to_token
      deployed
      { to_ = src () ;
        minTokensBought = 499248n ;
        deadline = future }
      1tz ;
    assert_dex_pools
      deployed
      { xtz_pool = 2tz ;
        token_pool = 500752n ;
        lqt_total = 1000000n } ;
    assert_tok deployed (src ()) 1000000499248n ;
    assert_lqt deployed (src ()) 1000000n
  end

let test_xtz_to_token_error_deadline =
  let deployed = basic_setup () in
  assert_error
    error_THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE
    (do_xtz_to_token
       deployed
       { to_ = src () ;
         minTokensBought = 499248n ;
         deadline = past }
       1tz)

let test_xtz_to_token_error_minTokensBought =
  let deployed = basic_setup () in
  assert_error
    error_TOKENS_BOUGHT_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_TOKENS_BOUGHT
    (do_xtz_to_token
       deployed
       { to_ = src () ;
         minTokensBought = 499249n ;
         deadline = future }
       1tz)


let test_xtz_to_token_error_selfIsUpdatingTokenPool =
  let deployed = basic_setup_selfIsUpdatingTokenPool () in
  assert_error
    error_SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE
    (do_xtz_to_token
       deployed
       { to_ = src () ;
         minTokensBought = 499248n ;
         deadline = future }
       1tz)

(* token to xtz *)

let test_token_to_xtz =
  let deployed = basic_setup () in
  begin
    assert_token_to_xtz
      deployed
      { to_ = src () ;
        tokensSold = 500000n ;
        minXtzBought = 332665mutez ;
        deadline = future } ;
    assert_dex_pools
      deployed
      { xtz_pool = 667335mutez ;
        token_pool = 1500000n ;
        lqt_total = 1000000n } ;
    assert_tok deployed (src ()) 999999500000n ;
    assert_lqt deployed (src ()) 1000000n
  end

let test_token_to_xtz_error_deadline =
  let deployed = basic_setup () in
  assert_error
    error_THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE
    (do_token_to_xtz
       deployed
       { to_ = src () ;
         tokensSold = 500000n ;
         minXtzBought = 332665mutez ;
         deadline = past }
       0tz )

let test_token_to_xtz_error_minXtzBought =
  let deployed = basic_setup () in
  assert_error
    error_XTZ_BOUGHT_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_XTZ_BOUGHT
    (do_token_to_xtz
       deployed
       { to_ = src () ;
         tokensSold = 500000n ;
         minXtzBought = 332666mutez ;
         deadline = future }
       0tz )

let test_token_to_xtz_error_selfIsUpdatingTokenPool =
  let deployed = basic_setup_selfIsUpdatingTokenPool () in
  assert_error
    error_SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE
    (do_token_to_xtz
       deployed
       { to_ = src () ;
         tokensSold = 500000n ;
         minXtzBought = 332665mutez ;
         deadline = future }
       0tz )

let test_token_to_xtz_error_amount =
  let deployed = basic_setup () in
  assert_error
    error_AMOUNT_MUST_BE_ZERO
    (do_token_to_xtz
       deployed
       { to_ = src () ;
         tokensSold = 500000n ;
         minXtzBought = 332665mutez ;
         deadline = future }
       1tz )

(* default *)

let test_default =
  let deployed = basic_setup () in
  let {dex_addr} = deployed in
  begin
    Test.transfer_to_contract_exn
      (Test.to_contract dex_addr)
      ()
      1tz ;
    ()
  end

let test_default_error_selfIsUpdatingTokenPool =
  let deployed = basic_setup_selfIsUpdatingTokenPool () in
  let {dex_addr} = deployed in
  assert_error
    error_SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE
    (Test.transfer_to_contract
     (Test.to_contract dex_addr)
     ()
     1tz)

(* set baker *)

let test_set_baker =
  let deployed = basic_setup () in
  let {dex_addr} = deployed in
  begin
    assert_set_baker
      deployed
      { baker = (None : key_hash option) ;
        freezeBaker = true } ;
    let storage = Test.get_storage dex_addr in
    if storage.freezeBaker then () else failwith "expected freezeBaker = true"
  end

let test_set_baker_error_selfIsUpdatingTokenPool =
  let deployed = basic_setup_selfIsUpdatingTokenPool () in
  assert_error
    error_SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE
    (do_set_baker
      deployed
      { baker = (None : key_hash option) ;
        freezeBaker = true }
      0tz)


let test_set_baker_error_amount =
  let deployed = basic_setup () in
  assert_error
    error_AMOUNT_MUST_BE_ZERO
    (do_set_baker
      deployed
      { baker = (None : key_hash option) ;
        freezeBaker = true }
      1tz)

let test_set_baker_error_manager =
  let deployed = basic_setup () in
  begin
    Test.set_source (other ()) ;
    assert_error
      error_ONLY_MANAGER_CAN_SET_BAKER
      (do_set_baker
        deployed
        { baker = (None : key_hash option) ;
          freezeBaker = true }
        0tz)
  end

let test_set_baker_error_frozen =
  let deployed = basic_setup () in
  let {dex_addr} = deployed in
  begin
    assert_set_baker
      deployed
      { baker = (None : key_hash option) ;
        freezeBaker = true } ;
    assert_error
      error_BAKER_PERMANENTLY_FROZEN
      (do_set_baker
        deployed
        { baker = (None : key_hash option) ;
          freezeBaker = true }
        0tz )
  end

(* set manager *)

let test_set_manager =
  let deployed = basic_setup () in
  let {dex_addr} = deployed in
  begin
    assert_set_manager
      deployed
      ("tz1fakefakefakefakefakefakefakcphLA5" : address) ;
    let storage = Test.get_storage dex_addr in
    if storage.manager = ("tz1fakefakefakefakefakefakefakcphLA5" : address)
    then ()
    else failwith "incorrect manager"
  end

let test_set_manager_error_selfIsUpdatingTokenPool =
  let deployed = basic_setup_selfIsUpdatingTokenPool () in
  assert_error
    error_SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE
    (do_set_manager
      deployed
      ("tz1fakefakefakefakefakefakefakcphLA5" : address)
      0tz)

let test_set_manager_error_amount =
  let deployed = basic_setup () in
  assert_error
    error_AMOUNT_MUST_BE_ZERO
    (do_set_manager
      deployed
      ("tz1fakefakefakefakefakefakefakcphLA5" : address)
      1tz)

let test_set_manager_error_manager =
  let deployed = basic_setup () in
  begin
    Test.set_source (other ()) ;
    assert_error
      error_ONLY_MANAGER_CAN_SET_MANAGER
      (do_set_manager
        deployed
        ("tz1fakefakefakefakefakefakefakcphLA5" : address)
        0tz)
  end

(* set lqt address *)
(* set_lqt_address was exercised during setup and is tested elsewhere *)

let test_set_lqt_address_error_selfIsUpdatingTokenPool =
  let deployed = basic_setup_selfIsUpdatingTokenPool () in
  assert_error
    error_SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE
    (do_set_lqt_address
      deployed
      ("tz1fakefakefakefakefakefakefakcphLA5" : address)
      0tz)

let test_set_lqt_address_error_amount =
  let deployed = basic_setup () in
  assert_error
    error_AMOUNT_MUST_BE_ZERO
    (do_set_lqt_address
      deployed
      ("tz1fakefakefakefakefakefakefakcphLA5" : address)
      1tz)

let test_set_lqt_address_error_already_set =
  let deployed = basic_setup () in
  assert_error
    error_LQT_ADDRESS_ALREADY_SET
    (do_set_lqt_address
      deployed
      ("tz1fakefakefakefakefakefakefakcphLA5" : address)
      0tz)

(* update token pool *)

let test_update_token_pool =
  let deployed = basic_setup () in
  let { dex_addr ; tok_addr } = deployed in
  begin
    Test.transfer_to_contract_exn
      (Test.to_entrypoint "mintOrBurn" tok_addr : mintOrBurn contract)
      { quantity = -1 ;
        target = Tezos.address (Test.to_contract dex_addr) }
      0tz ;
    assert_dex_pools
      deployed
      { xtz_pool = 1tz ;
        token_pool = 1000000n ;
        lqt_total = 1000000n } ;
    assert_update_token_pool deployed ;
    assert_dex_pools
      deployed
      { xtz_pool = 1tz ;
        token_pool = 999999n ;
        lqt_total = 1000000n }
  end

let test_update_token_pool_error_selfIsUpdatingTokenPool =
  let deployed = basic_setup_selfIsUpdatingTokenPool () in
  let { dex_addr } = deployed in
  assert_error
    error_UNEXPECTED_REENTRANCE_IN_UPDATE_TOKEN_POOL
    (do_update_token_pool deployed 0tz)

let test_update_token_pool_error_amount =
  let deployed = basic_setup_selfIsUpdatingTokenPool () in
  let { dex_addr } = deployed in
  assert_error
    error_AMOUNT_MUST_BE_ZERO
    (do_update_token_pool deployed 1tz)

let test_update_token_pool_error_non_implicit =
  let originated_proxy_main (c, storage : unit contract * unit) : operation list * unit =
      let op = Tezos.transaction () 0mutez c in
      ([op], ()) in
  let deployed = basic_setup () in
  let { dex_addr } = deployed in
  begin
    let (proxy_addr, _, _) = Test.originate originated_proxy_main () 0tz in
    assert_error
      error_CALL_NOT_FROM_AN_IMPLICIT_ACCOUNT
      (Test.transfer_to_contract
        (Test.to_contract proxy_addr : (unit contract) contract)
        (Test.to_entrypoint "updateTokenPool" dex_addr : unit contract)
        0tz)
  end

(* token to token *)

let test_token_to_token =
  let deployed = basic_setup () in
  let {dex_addr} = deployed in
  begin
    assert_token_to_token
      deployed
      { outputDexterContract = Tezos.address (Test.to_contract dex_addr) ;
        minTokensBought = 497997n ;
        to_ = src () ;
        tokensSold = 500000n ;
        deadline = future } ;
    assert_dex_pools
      deployed
      { xtz_pool = 1tz ;
        token_pool = 1002003n ;
        lqt_total = 1000000n } ;
    assert_tok deployed (src ()) 999999997997n ;
    assert_lqt deployed (src ()) 1000000n
  end

let test_token_to_token_error_selfIsUpdatingTokenPool =
  let deployed = basic_setup_selfIsUpdatingTokenPool () in
  let { dex_addr } = deployed in
  assert_error
    error_SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE
    (do_token_to_token
      deployed
      { outputDexterContract = Tezos.address (Test.to_contract dex_addr) ;
        minTokensBought = 497997n ;
        to_ = src () ;
        tokensSold = 500000n ;
        deadline = future }
      0tz)

let test_token_to_token_error_amount =
  let deployed = basic_setup () in
  let { dex_addr } = deployed in
  assert_error
    error_AMOUNT_MUST_BE_ZERO
    (do_token_to_token
      deployed
      { outputDexterContract = Tezos.address (Test.to_contract dex_addr) ;
        minTokensBought = 497997n ;
        to_ = src () ;
        tokensSold = 500000n ;
        deadline = future }
      1tz)

let test_token_to_token_error_deadline =
  let deployed = basic_setup () in
  let { dex_addr } = deployed in
  assert_error
    error_THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE
    (do_token_to_token
      deployed
      { outputDexterContract = Tezos.address (Test.to_contract dex_addr) ;
        minTokensBought = 497997n ;
        to_ = src () ;
        tokensSold = 500000n ;
        deadline = past }
      0tz)
